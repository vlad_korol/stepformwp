<?php

/**
 * this class for view form in frontend
 *
 * @author Goru
 */

namespace GoruStepChoose\StepChoose;

class View {

    /**
     * 
     * $QUESTIONS[0] = array('question'=>'Question content',
     *                        'answers'=>array(
     *                          'Yes'=>'action_1',
     *                          'No'=>'action_2')
     *                 );
     * 
     * @var array $QUESTIONS containt array of questions 
     */
    private $QUESTIONS;

    /**
     *
     * @var array('Yes', 'No');
     */
    private $ANSWERS_DEFAULT;

    function __construct() {
        add_action('sf_html', array($this, 'generateHtml'));
        add_shortcode('step-form', array($this, 'generateHtml'));
    }

    /**
     * Function for adding new question to current 
     * questions array
     * @param type $data
     * @return boolean 
     */
    public function addQuestion($question, $answers) {

        // Create our array of values
        // First, sanitize the data and remove white spaces
        $no_whitespaces_answers = preg_replace('/\s*,\s*/', ',', filter_var($answers, FILTER_SANITIZE_STRING));
        $answers_array = explode('|', $no_whitespaces_answers);

        /**
         * check for question
         */
        if (empty($question)) {
            return FALSE;
        }

        if (!empty($answers_array) && is_array($answers_array)) {
            foreach ($answers_array as $key => $value) {
                if ($key % 2 == 0) {
                    $_answers[$value] = !empty($answers_array[$key + 1]) ? $answers_array[$key + 1] : "";
                }
            }
        } else {
            $_answers = $this->ANSWERS_DEFAULT;
        }

        $this->QUESTIONS[] = array(
            'question' => $question,
            'answers' => $_answers
        );

        return TRUE;
    }

    public function getQuestions($num = 0) {
        if (int($num) >= 1) {
            return $this->QUESTIONS[$num];
        } else {
            return $this->QUESTIONS;
        }
    }

    /**
     * 
     * @param array || null 
     * if array then $arg key can contain:
     * class = wraper container class
     * showStep = show or hide steps
     * 
     * 
     * @return string - html
     */
    public function generateHtml($arg = false) {

        $arguments = shortcode_atts(
                array(
            'shownav' => true,
            'showstep' => true,
            'navname' => false,
            'class' => '',
            'question_1' => '',
            'answers_1' => '',
            'media_1' => '',
            'question_2' => '',
            'answers_2' => '',
            'media_2' => '',
            'question_3' => '',
            'answers_3' => '',
            'media_3' => '',
                ), $arg
        );

        if (!empty($arguments['question_1']) && !empty($arguments['answers_1'])) {
            $this->addQuestion($arguments['question_1'], $arguments['answers_1']);
        }
        if (!empty($arguments['question_2']) && !empty($arguments['answers_2'])) {
            $this->addQuestion($arguments['question_2'], $arguments['answers_2']);
        }
        if (!empty($arguments['question_3']) && !empty($arguments['answers_3'])) {
            $this->addQuestion($arguments['question_3'], $arguments['answers_3']);
        }

        $nav_count = count($this->QUESTIONS);

        $result = sprintf('<div id="sf" class="%s">', $arguments['class']);
        if (empty($arguments['navname'])) {
            $result .= $this->getStepsNav($nav_count);
        } else {
            $result .= $this->getStepsNav($nav_count, $arguments['navname']);
        }

        $result .= '<ul class="steps-main">';
        foreach ($this->QUESTIONS as $q_num => $value) {
            $cur_num = $q_num + 1;
            $result .= '<li class="step-block">';
            if (!empty($arguments['showstep'])) {
                $result .= sprintf('<span class="step-num">Step %d of %d</span>', $cur_num, count($this->QUESTIONS));
            }

            if (!empty($arguments["media_{$cur_num}"])) {
                $media = $arguments["media_{$cur_num}"];
                $pathInfo = pathinfo($media);
                // var_dump(pathinfo('http://wordpress.loc/wp-content/uploads/2016/03/1622752010051729711752.jpeg'), PATHINFO_EXTENSION);
                var_dump($media, pathinfo($media), PATHINFO_EXTENSION);
                if (exif_imagetype($media) || !empty($pathInfo['extension'])) {
                    $result .= sprintf('<figure><img src="%s" alt="question %d img" /></figure>', $arguments["media_{$cur_num}"], $cur_num);
                } else {
                    $result .= '<div class="video">' . $media . '</div>';
                }
            }

            $result .= sprintf('<div class="sf-content">%s</div>', $value['question']);
            $result .= '<div class="answers button-group">';
            $i = 0;
            foreach ($value['answers'] as $answer_text => $answer_link) {
                $link = $answer_link === true ? 'next' : $answer_link;
                $result .= sprintf('<button class="btn" data-url="%s">%s</button>', $link, $answer_text);
            }
            $result .= '</div>';
            $result .= '</li>';
        }
        $result .= '</ul>';
        $result .= '</div>';

        return $result;
    }

    public function getStepsNav($num = 3, $nav_name = 'One step|Two step|Three step') {

        $no_whitespaces_nav_name = preg_replace('/\s*,\s*/', ',', filter_var($nav_name, FILTER_SANITIZE_STRING));
        $nav_name_array = explode('|', $no_whitespaces_nav_name);

        $result = '
            <div class="sf-nav-wrap clearfix sf-nav-smmob sf-nav-bottom">
            <ul id="progressbar" class="sf-nav clearfix" style="clear: both;">';

        for ($i = 1; $i <= $num; $i++) {
            $result .= '<li class="sf-nav-step sf-li-number sf-nav-link">
            <span class="sf-nav-subtext">' . $nav_name_array[$i - 1] . '</span>
            <div class="sf-nav-number">
            <span class="sf-nav-number-inner">' . $i . '</span>
            </div>
            </li>';
        }

        $result .= '
            </ul>
            </div>
            ';
        return $result;
    }

}
