<?php

/*
 * ******************************************************************
 * Copyright (c) 2014 Pierre Beitz <pb@naymspace.de>, naymspace
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * ******************************************************************
 */

namespace GoruStepChoose\StepChoose;

class Backend {

    function __construct() {

        add_action('admin_enqueue_scripts', array($this, 'register_assets'));
        add_action('admin_head', array($this, 'add_mce_button'));
        add_action('admin_menu', array($this, 'plugin_menu'));
    }

    // displays plugins menu in wordpress' toolbar, TODO: alter the second string, if you want a more readable name in the backend
    function plugin_menu() {
        add_options_page('StepChoose', 'StepChoose', 'manage_options', 'GoruStepChoose\StepChoose-options', function () {

            if (!current_user_can('manage_options')) {
                wp_die(__('Sie besitzen nicht die Rechte zum Bearbeiten.'));
            }

            // handle posted options
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->handle_options();
            }

            // display plugin's options page
            Helper::render_template('admin/options');
        });
    }

    /*
     * This function added buttom to TinyMCE
     */

    // Hooks your functions into the correct filters
    function add_mce_button() {
        // check user permissions
        if (!current_user_can('edit_posts') && !current_user_can('edit_pages')) {
            return;
        }
        // check if WYSIWYG is enabled
        if ('true' == get_user_option('rich_editing')) {
            add_filter('mce_external_plugins', array($this, 'add_tinymce_plugin'));
            add_filter('mce_buttons', array($this, 'register_mce_button'));
        }
    }

    // Declare script for new button
    function add_tinymce_plugin($plugin_array) {
        $plugin_array['step_form_button'] = plugins_url("/../scripts/mce_button.js", __FILE__);
        return $plugin_array;
    }

    // Register new button in the editor
    function register_mce_button($buttons) {
        array_push($buttons, 'step_form_button');
        return $buttons;
    }

    // load your scripts and styles
    function register_assets() {
        //$this->add_script('mce_button', array('jquery'));
    }

    private function add_style($name) {
        wp_register_style(__NAMESPACE__ . "-style-{$name}", plugins_url("/../stylesheets/{$name}.css", __FILE__));
        wp_enqueue_style(__NAMESPACE__ . "-style-{$name}");
    }

    private function add_script($name, $dependencies = array(), $version = "1.0", $include_in_footer = true) {
        wp_register_script(__NAMESPACE__ . "-script-{$name}", plugins_url("/../scripts/{$name}.js", __FILE__), $dependencies, $version, $include_in_footer);
        wp_enqueue_script(__NAMESPACE__ . "-script-{$name}");
    }

    // handle posted option-values
    function handle_options() {
        foreach (Base::$options as $option) {
            $id = $option['id'];
            $new_value = isset($_POST[$id]) ? $_POST[$id] : null;

            switch ($option['type']) {
                case 'text':
                    if (isset($new_value))
                        update_option($id, $new_value);
                    break;
                case 'checkbox':
                    update_option($id, isset($new_value));
                    break;
                case 'select':
                    if (isset($new_value))
                        update_option($id, $new_value);
                    break;
            }
        }
    }

    public static function render_option($option) {
        switch ($option['type']) {
            case 'text':
                Helper::render_template('admin/helpers/form_label', $option);
                Helper::render_template('admin/helpers/form_text', $option);
                break;

            case 'checkbox':
                Helper::render_template('admin/helpers/form_label', $option);
                Helper::render_template('admin/helpers/form_checkbox', $option);
                break;

            case 'select':
                Helper::render_template('admin/helpers/form_label', $option);
                Helper::render_template('admin/helpers/form_select', $option);
                break;
        }
    }

}
