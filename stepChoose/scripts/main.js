jQuery(document).ready(function ($) {

//    $('.one-time').slick({
//        dots: true,
//        infinite: true,
//        speed: 300,
//        slidesToShow: 1,
//        adaptiveHeight: true
//    });



    //jQuery time
    var current_fs, current_fs_index, next_fs, previous_fs; //fieldsets
    var left, opacity, scale; //fieldset properties which we will animate
    var animating; //flag to prevent quick multi-click glitches

    $("#sf .step-block:first").addClass("sf-active");
    $("#progressbar .sf-nav-step:first").addClass("sf-active");


    $("#sf .answers button").click(function () {
        var $this = $(this);

        if (animating)
            return false;
        animating = true;

        var $url = $(this).data('url');

        if ($url === "next") {
        current_fs = $this.closest(".step-block");
                current_fs_index = current_fs.index();
                next_fs = current_fs.next();
                //activate next step on progressbar using the index of next_fs
                $("#progressbar li").eq($(".step-block").index(next_fs)).addClass("sf-active");
                current_fs.css({"position": "absolute"});
                //show the next fieldset
                next_fs.show();
                //hide the current fieldset with style
                current_fs.animate({opacity: 0}, {
                step: function (now, mx) {
                    //as the opacity of current_fs reduces to 0 - stored in "now"
                    //1. scale current_fs down to 80%
                    scale = 1 - (1 - now) * 0.2;
                    //2. bring next_fs from the right(50%)
                    left = (now * 50) + "%";
                    //3. increase opacity of next_fs to 1 as it moves in
                    opacity = 1 - now;
                    current_fs.css({'transform': 'scale(' + scale + ')'});
                    next_fs.css({'left': left, 'opacity': opacity});
                },
                        duration: 800,
                        complete: function () {
                            current_fs.hide();
                            animating = false;
                        },
                        //this comes from the custom easing plugin
                        easing: 'easeInOutBack'
                });
        } else if ($url === "confirm") {
            location = '/?stepChoose=1';
        } else {
        location = $url;
    }


    });
            $(".previous").click(function () {
        if (animating)
            return false;
        animating = true;

        current_fs = $(this).parent();
        previous_fs = $(this).parent().prev();

        //de-activate current step on progressbar
        $("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("sf-active");

        //show the previous fieldset
        previous_fs.show();
        //hide the current fieldset with style
        current_fs.animate({opacity: 0}, {
            step: function (now, mx) {
                //as the opacity of current_fs reduces to 0 - stored in "now"
                //1. scale previous_fs from 80% to 100%
                scale = 0.8 + (1 - now) * 0.2;
                //2. take current_fs to the right(50%) - from 0%
                left = ((1 - now) * 50) + "%";
                //3. increase opacity of previous_fs to 1 as it moves in
                opacity = 1 - now;
                current_fs.css({'left': left});
                previous_fs.css({'transform': 'scale(' + scale + ')', 'opacity': opacity});
            },
            duration: 800,
            complete: function () {
                current_fs.hide();
                animating = false;
            },
            //this comes from the custom easing plugin
            easing: 'easeInOutBack'
        });
    });

//    $(".redirect").on('click', function () {
//        var $url = $(this).data('url');
//        location = $url;
//    });


//    $(".submit").click(function () {
//        return false;
//    });

});