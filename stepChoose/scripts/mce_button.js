(function () {




    /* Register the buttons */

    tinymce.PluginManager.add('step_form_button', function (editor, url) {

        var self = this, button, $textbox;

        var dynamicallyEditable;

        editor.addButton('step_form_button', {
            text: 'Add step form',
            icon: 'mce-ico dashicons-before dashicons-welcome-widgets-menus',
            onclick: function () {
                editor.windowManager.open({
                    title: 'Insert Step Form',
                    body: [
                        // вставить bool nav
                        // вставить буль показывать шаги
                        {
                            type: 'listbox',
                            name: 'show_nav',
                            label: 'Show nav block',
                            'values': [
                                {text: 'Yes', value: 'true'},
                                {text: 'No', value: 'false'}
                            ]
                        },
                        {
                            type: 'listbox',
                            name: 'show_step_count',
                            label: 'Show step count',
                            'values': [
                                {text: 'Yes', value: 'true'},
                                {text: 'No', value: 'false'}
                            ]
                        },
                        {
                            type: 'textbox',
                            name: 'nav_name',
                            label: 'Nav items name:',
                            value: 'One|Two|Three'
                        },
                        {
                            type: 'textbox',
                            name: 'question_1',
                            label: 'Question one:',
                            value: 'Text for first question',
                            multiline: true,
                            minWidth: 300,
                            minHeight: 50
                        },
                        {
                            type: 'textbox',
                            name: 'media_1',
                            label: 'Media for first step:',
                            value: ''
                        },
                        {
                            type: 'textbox',
                            name: 'answers_1',
                            label: 'Answer one(use "next" to next step<br> end "confirm" to unblock site):',
                            value: 'Yes|next|No|http://google.com'
                        },
                        {
                            type: 'textbox',
                            name: 'question_2',
                            label: 'Question two:',
                            value: '',
                            multiline: true,
                            minWidth: 300,
                            minHeight: 50
                        },
                        {
                            type: 'textbox',
                            name: 'media_2',
                            label: 'Media for second step:',
                            value: ''
                        },
                        {
                            type: 'textbox',
                            name: 'answers_2',
                            label: 'Answer two:',
                            value: ''
                        },
                        {
                            type: 'textbox',
                            name: 'question_3',
                            label: 'Question three:',
                            value: '',
                            multiline: true,
                            minWidth: 300,
                            minHeight: 50
                        },
                        {
                            type: 'textbox',
                            name: 'media_3',
                            label: 'Media for third step:',
                            value: ''
                        },
                        {
                            type: 'textbox',
                            name: 'answers_3',
                            label: 'Answer three:',
                            value: ''
                        }
                    ],
                    onsubmit: function (e) {
                        console.log(e);
                        editor.insertContent('[step-form shownav="' + e.data.show_nav +
                                '" navname="' + e.data.nav_name +
                                '" showstep="' + e.data.show_step_count +
                                '" question_1="' + e.data.question_1 +
                                '" media_1="' + e.data.media_1 +
                                '" answers_1="' + e.data.answers_1 +
                                '" question_2="' + e.data.question_2 +
                                '" media_2="' + e.data.media_2 +
                                '" answers_2="' + e.data.answers_2 +
                                '" question_3="' + e.data.question_3 +
                                '" media_3="' + e.data.media_3 +
                                '" answers_3="' + e.data.answers_3 +
                                '"]');
                    }
                });
            }
        });
    });


})();


