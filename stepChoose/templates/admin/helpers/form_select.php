<td>
    <?php
    // var_dump($option, get_option($option["id"]));
    ?>
    <select name='<?php echo $option["id"] ?>' id='<?php echo $option["id"] ?>' class="<?php echo $option["class"] ?>">
        <?php
        foreach ($option["values"] as $value => $name) {
            printf('<option %s value="%s">%s</option>', selected($value, get_option($option["id"]), false), $value, $name);
        }
        ?>
    </select>
</td>