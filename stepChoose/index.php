<?php

/*
  Plugin Name: StepChoose
  Version: 0.0.6
  Plugin URI: http://v-goru.com/plugins/step_form
  Description: Plugin for show step form via shortcode.
  Author: Goru
  Author URI: http://v-goru.com
  Author Email: web.goru@gmail.com
 */

namespace GoruStepChoose\StepChoose;

class Base {

// options to be set in the admin-area

    public static $options = array(
        'Select desing' => array(
            'id' => 'GoruStepChoose\StepChoose-design',
            'title' => 'Select style for block',
            'type' => 'select',
            'values' => array(
                'light' => "Light",
                'dark' => "Dark"),
            'default' => 'dark'
        ),
        'Set redirect link' => array(
            'id' => 'GoruStepChoose\StepChoose-redirectPage',
            'title' => 'Set url to redirect page',
            'type' => 'text',
            'default' => ''
        ),
        'Use redirection' => array(
            'id' => 'GoruStepChoose\StepChoose-use-redirection',
            'title' => 'Enable redirection',
            'type' => 'checkbox',
            'default' => 'on'
        )
    );

    function __construct() {
        spl_autoload_register(array($this, 'plugin_autoloader'));
        register_activation_hook(__FILE__, array($this, 'activate_plugin'));

        if (Helper::in_backend()) {
            new Backend();
        }

        if (!Helper::in_backend()) {
            new Frontend();
            new View();
        }

        new Database();
    }

    function plugin_autoloader($class) {
        $dir = dirname(__FILE__);
        $namespace = explode('\\', $class);
        $className = $namespace[count($namespace) - 1]; // this feels very hacky. any suggestions?

        if (file_exists("{$dir}/modules/{$className}.php")) {
            require_once "{$dir}/modules/{$className}.php";
        }
    }

    function activate_plugin() {
        foreach (Base::$options as $option) {
            if ($option['default'] && null !== (get_option($option['id']))) {
                update_option($option['id'], $option['default']);
            }
        }
    }

}

session_start();

//$_SESSION['stepChoose'] = 0;

if (isset($_GET['stepChoose']) && $_GET['stepChoose'] == 1) {
    $_SESSION['stepChoose'] = $_GET['stepChoose'];
}

/**
 * If user not logged and form not confirm then - redirect to form page
 * else if user logged and url is form url - redirect to home page
 */
if ((!isset($_SESSION['stepChoose']) || $_SESSION['stepChoose'] != 1) && is_user_logged_in() == false) {

    add_action('template_redirect', function() {
        $url = get_option('GoruStepChoose\StepChoose-redirectPage');
        $redirect = get_option('GoruStepChoose\StepChoose-use-redirection');

        if (!empty($url) && preg_match('#^' . $url . '?$#i', $_SERVER['REQUEST_URI'])) {
            
        } elseif (!empty($redirect)) {
            wp_redirect($url, 301);
            exit;
        }
    });
}

new Base();

